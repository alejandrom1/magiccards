import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStylesCustom = makeStyles(theme => ({
    root: {
        position: "relative",
        display: "flex",
        justifyContent: "center",
        marginTop: "50px"
    },
    bottom: {
        color: theme.palette.grey[theme.palette.type === "light" ? 700 : 700]
    },
    top: {
        color: "#1a90ff",
        animationDuration: "800ms",
        position: "absolute"
    },
    
}));

function CustomCircularProgress(props) {
    const classes = useStylesCustom();

    return (
        <div className={classes.root}>
            <CircularProgress
                variant="determinate"
                className={classes.bottom}
                size={70}
                thickness={2}
                {...props}
                value={100}
            />
            <CircularProgress
                variant="indeterminate"
                disableShrink
                className={classes.top}
                size={70}
                thickness={2}
                {...props}
            />
        </div>
    );
}

const useStyles = makeStyles({
    root: {
        flexGrow: 1
    }
});

export default function Cargando() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <CustomCircularProgress />
        </div>
    );

}
