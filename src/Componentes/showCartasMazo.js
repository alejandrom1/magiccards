import React, { Component } from 'react';
import { MAZO_KEY, USER_KEY } from '../conf/const';
import { sets } from '../conf/routes';
import { Link } from 'react-router-dom';
import "./PrintarCards.css";
import "./showCartasMazo.css";

class showCartasMazo extends Component {

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));
       
        if (usuario !== null) return usuario.id;
        else return null;
    }

    _eliminarCartaMazo = (carta, idMazo) => {
        let idUser = this._getUsuarioId();

        let mazoEntero = JSON.parse(localStorage.getItem(MAZO_KEY));


        mazoEntero.forEach(elemento => {
            if (idUser === elemento.idUsuario) {
                
                if (elemento.mazos.filter(el => el.id === idMazo).length > 0) {
                    elemento.mazos.forEach((mazo) => {
                        
                        if (mazo.id === idMazo) {
                           
                            mazo.cartas.forEach((card, indice) => {
                                if (carta.id === card.id) {
                                    mazo.cartas.splice(indice, 1);
                                    localStorage.setItem(MAZO_KEY, JSON.stringify(mazoEntero));

                                }
                            })
                        }
                    })
                }
            }
        })

    }


    render() {
        let ruta = window.location.pathname;

        const idLink = this.props.match.params.idMazo * 1;

        let idUser = this._getUsuarioId();
        const objetoMazo = JSON.parse(localStorage.getItem(MAZO_KEY))

        if (objetoMazo !== null) {
            let checkMazos = objetoMazo.map(el => el.idUsuario);
            let checkMazos2 = checkMazos.includes(idUser);
            if (checkMazos2 !== false) {
                let userListado = objetoMazo.filter(listado => listado.idUsuario === idUser);
                if (userListado !== undefined) {
                    if (idUser === userListado[0].idUsuario) {
                        let arrayMazos = userListado[0].mazos;
               
                        let ubicacionMazoActual = arrayMazos.filter(mazo => mazo.id === idLink);
                    
                        let valorMazoActual = ubicacionMazoActual[0];
                       

                        return (
                            <div className="">
                                {objetoMazo !== null
                                    ? (
                                        valorMazoActual !== undefined
                                            ? (
                                                <div>
                                                    <div className="name-set">
                                                        <Link to={sets()} className="btn btn-danger position-relative ml-4">Volver</Link>
                                                        <h2 className="text-center mt-2">{valorMazoActual.nombre}</h2>
                                                    </div>
                                                    <div className="cards">
                                                        {valorMazoActual.cartas !== undefined
                                                            ? valorMazoActual.cartas.length > 0
                                                                ? (valorMazoActual.cartas.map((carta, index) => {

                                                                    return (

                                                                        <div key={index} className="card">
                                                                                <p className="m-0 p-2">{carta.name}</p>
                                                                                <div className="d-flex align-self-center p-2" style={{width: '240px'}}>
                                                                                    <img className="img-fluid" style={{width: '100%'}} alt={carta.name} src={carta.imageUrl === undefined ? "https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" : carta.imageUrl} />
                                                                                </div>
                                                                                <Link to={ruta} className="btn btn-danger mt-2 w-100" onClick={() => this._eliminarCartaMazo(carta, valorMazoActual.id)}>Eliminar</Link>
                                                                        </div>

                                                                    );
                                                                }))
                                                                : <p>No hay cartas en este mazo...</p>
                                                            : <p>No hay cartas en este mazo...</p>
                                                        }
                                                    </div>
                                                </div>
                                            )
                                            : (
                                                <div>
                                                    <div className="name-set">
                                                        <Link to={sets()} className="btn btn-danger position-relative ml-4 mb-2">Volver</Link>
                                                    </div>
                                                    <p className="col mt-4 text-center">No tienes ningún mazo creado con este Id</p>
                                                </div>
                                            )

                                    )
                                    : (
                                        <div>
                                            <div className="name-set">
                                                <Link to={sets()} className="btn btn-danger position-relative ml-4">Volver</Link>
                                            </div>
                                            <p className="col mt-4">No tienes ningún mazo creado</p>
                                        </div>
                                    )
                                }

                            </div>
                        );
                    }
                }
            }
        }

    }
}

export default showCartasMazo;