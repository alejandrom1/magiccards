import React, { Component } from 'react';
import "./Comparar_Cartas.css";

import ProgressBar from 'react-customizable-progressbar';


import { Compara_KEY, USER_KEY } from '../conf/const';

export default class Compara_Cartas extends Component {

    constructor() {
        super();
        this.state = {
            carta1: [],
            carta2: [],
            recibido2: false,
            recibido1: false,
        }

    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) {
            return usuario.id
        }

        return null;
    }
    _getCartas(cartas) {
        let idUsuario = this._getUsuarioId();
        let cartitas = cartas.filter(element => element.idUsuario === idUsuario).map(element => element.carta);
        return cartitas.length === 0 ? cartitas : cartitas[0];
    }

    _mensajeError = (mensaje) => {
        return <h2 className="text-center">{mensaje}</h2>
    }

    render() {

        if (localStorage.getItem(Compara_KEY) === null) {
            return this._mensajeError("No hay cartas para comparar");
        }

        let cartas = JSON.parse(localStorage.getItem(Compara_KEY));

        if (cartas.length < 1) {
            return this._mensajeError("No hay cartas para comparar");
        }

        let array_cartas = this._getCartas(cartas);

        if (array_cartas.length === 0) {
            return this._mensajeError("No hay cartas para comparar");
        }

        let carta1 = array_cartas[0];
        let carta2 = array_cartas[1];
        let tipo_carta1 = {};
        let tipo_carta2 = {};

        if (carta1 !== undefined) {
            tipo_carta1 = carta1.types[0];
        } if (carta2 !== undefined) {
            tipo_carta2 = carta2.types[0];
        }

        if (carta1 === undefined || carta2 === undefined) {
            return this._mensajeError("No hay suficientes cartas para comparar");
        }


        return (
            <div className="compara-carta p-4">

                <div className="text-center">
                    <h1 className="vs">VS</h1>
                </div>

                <div className="row text-center">
                    <div className="col">
                        <h3 className="p-3">{carta1.name}</h3>
                    </div>
                    <div className="col">
                        <h3 className="p-3">{carta2.name}</h3>
                    </div>
                </div>

                <div className="row text-center">
                    <div className="col d-flex justify-content-center">
                        <div style={{ width: '240px' }}>
                            {carta1.imageUrl === undefined
                                ? <img className="img-fluid" alt={carta1.name} src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                                : <img className="img-fluid w-100" alt={carta1.name} src={carta1.imageUrl} />
                            }
                        </div>
                    </div>
                    <div className="col d-flex justify-content-center">
                        <div style={{ width: '240px' }}>
                            {carta2.imageUrl === undefined
                                ? <img className="img-fluid" alt={carta2.name} src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                                : <img className="img-fluid w-100" alt={carta2.name} src={carta2.imageUrl} />
                            }
                        </div>
                    </div>
                </div>

                <div className="row text-center">
                    <div className="col">
                        <p>{tipo_carta1}</p>
                    </div>
                    <div className="col">
                        <p>{tipo_carta2}</p>
                    </div>
                </div>

                <div className="row">

                    <div className="col d-flex flex-column align-items-center">
                        <div className="datos-comparar">
                            <div className="row text-center">
                                <div className="col">Coste</div>
                                <div className="col">Fuerza</div>
                                <div className="col">Resistencia</div>
                            </div>

                            <div className="row text-center">

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta1.cmc}
                                        strokeWidth={5}
                                        strokeColor="#0CFF00"
                                        steps={10}
                                        cut={120}
                                        rotate={-210}
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta1.cmc}</span>
                                    </ProgressBar>
                                </div>

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta1.power}
                                        steps={20}
                                        cut={120}
                                        rotate={-210}
                                        strokeColor="#00B9FF"
                                        strokeWidth={5}
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta1.power}</span>
                                    </ProgressBar>
                                </div>

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta1.toughness}
                                        steps={20}
                                        cut={120}
                                        rotate={-210}
                                        strokeColor="#FBFF00"
                                        strokeWidth={5}
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta1.toughness}</span>
                                    </ProgressBar>
                                </div>
                            </div>

                            <div className="row text-center">
                                <div className="col">
                                    {/* comparaciones coste */}
                                    {carta1.cmc > carta2.cmc
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.cmc < carta2.cmc
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.cmc === carta1.cmc
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                                <div className="col">
                                    {/* comparaciones fuerza */}
                                    {carta1.power > carta2.power
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.power < carta2.power
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.power === carta2.power
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                                <div className="col">
                                    {/* comparación resistencia */}
                                    {carta1.toughness > carta2.toughness
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.toughness < carta2.toughness
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.toughness === carta2.toughness
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                            </div>

                        </div>

                    </div>
                    <div className="col d-flex flex-column align-items-center">
                        <div className="datos-comparar">
                            <div className="row text-center">
                                <div className="col">Coste</div>
                                <div className="col">Fuerza</div>
                                <div className="col">Resistencia</div>
                            </div>

                            <div className="row text-center">

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta2.cmc}
                                        strokeWidth={5}
                                        strokeColor="#0CFF00"
                                        steps={10}
                                        rotate={-210}
                                        cut={120}
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta2.cmc}</span>
                                    </ProgressBar>
                                </div>

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta2.power}
                                        steps={20}
                                        rotate={-210}
                                        cut={120}
                                        strokeWidth={5}
                                        strokeColor="#00B9FF"
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta2.power}</span>
                                    </ProgressBar>
                                </div>

                                <div className="col">
                                    <ProgressBar
                                        radius={20}
                                        progress={carta2.toughness}
                                        steps={20}
                                        cut={120}
                                        rotate={-210}
                                        strokeWidth={5}
                                        strokeColor="#FBFF00"
                                        strokeLinecap="round"
                                        trackStrokeWidth={3}
                                    >
                                        <span className="indicator">{carta2.toughness}</span>
                                    </ProgressBar>
                                </div>
                            </div>
                            <div className="row text-center">
                                <div className="col">
                                    {/* comparaciones coste */}
                                    {carta2.cmc > carta1.cmc
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.cmc < carta1.cmc
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta1.cmc === carta2.cmc
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                                <div className="col">
                                    {/* comparaciones fuerza */}
                                    {carta2.power > carta1.power
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.power < carta1.power
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.power === carta1.power
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                                <div className="col">
                                    {/* comparación resistencia */}
                                    {carta2.toughness > carta1.toughness
                                        ? <i className="fa fa-chevron-up iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.toughness < carta1.toughness
                                        ? <i className="fa fa-chevron-down iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                    {carta2.toughness === carta1.toughness
                                        ? <i className="fa fa-minus iconos-comparativa" aria-hidden="true" />
                                        : ""
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}