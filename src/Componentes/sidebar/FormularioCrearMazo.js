import React from 'react';
import '../../globales/estilos.css';
import { MAZO_KEY, USER_KEY } from '../../conf/const';
import { Link } from 'react-router-dom';

class FormularioCrearMazo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            nombre: '',
            error: ''
        }

    }

    _getMazos = () => {
        return JSON.parse(localStorage.getItem(MAZO_KEY));
    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) {
            return usuario.id
        }

        return null;
    }

    _handleValidation = () => {
        
        let valid = true;
        let nombre = this.state.nombre;

        if(nombre.trim()===""){
            valid=false;
        }

        if(!valid){
            this.setState({
                error: 'El campo no puede ser vacio'
            })
        }

        return valid;

    }

    _handleSubmit = () => {
        if(this._handleValidation()){
            this._setMazo();
        }
    }

    _setMazo = () => {
        const nombre = this.state.nombre;

        let mazos = this._getMazos();
        let idUsuario = this._getUsuarioId();
        let usuarioNoExiste = true;

        //Si el localstorage de mazo no existe crea uno nuevo
        if (mazos === null && idUsuario !== null) {
            let nuevoMazo = [{ idUsuario: idUsuario, mazos: [{ id: 1, nombre: nombre, cartas: [] }] }];

            this.props.nuevo_mazo(nuevoMazo[0].mazos[0]);
            localStorage.setItem(MAZO_KEY, JSON.stringify(nuevoMazo));

            //Si el localstorage de mazo ya existe, comprueba si el usuario tiene mazos creados
            //Si el usuario tiene mazos creados hará el push del nuevo mazo al usuario sino creara un usuario nuevo con el nuevo mazo
        } else if (mazos !== null && idUsuario !== null) {
            mazos.forEach(el => {
                if (idUsuario === el.idUsuario) {
                    usuarioNoExiste = false;
                    let idMazos = el.mazos.map(el => el.id);
                    let nuevoId = idMazos.length>0 ? Math.max(...idMazos) + 1 : 1;
                    let nuevoMazo = { id: nuevoId, nombre: nombre, cartas: [] };
                    el.mazos.push(nuevoMazo);

                    this.props.nuevo_mazo(nuevoMazo);
                    localStorage.setItem(MAZO_KEY, JSON.stringify(mazos));
                }
            })

            if (usuarioNoExiste) {
                let nuevoMazo = { idUsuario: idUsuario, mazos: [{ id: 1, nombre: nombre, cartas: [] }] };
                mazos.push(nuevoMazo);

                this.props.nuevo_mazo(nuevoMazo.mazos[0]);
                localStorage.setItem(MAZO_KEY, JSON.stringify(mazos));
            }

        }

        this.setState({nombre: '', error: ''})

    }

    render() {
        let ruta = window.location.pathname;

        return (
            <div className="row mt-3">
                <div className="col-10">
                    <input type="text"
                        className="form-control"
                        placeholder="Nuevo mazo"
                        value={this.state.nombre}
                        onChange={(e) => this.setState({ nombre: e.target.value })} />
                    <small className="text-danger">{this.state.error}</small>
                </div>
                <div className="col-1">
                    <Link to={ruta} onClick={() => this._handleSubmit()} className="btn btn-primary" title="Añadir mazo"><i className="fa fa-plus"></i></Link>
                </div>
            </div>
        );
    }
}

export default FormularioCrearMazo;