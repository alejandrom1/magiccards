import React from 'react';
import './Sidebar.css';
import '../Menu.css';
import Acordion from './Acordion';
import MazosSidebar from './MazosSidebar';
import CartasSidebar from './CartasSidebar';
import ListaCartasComparar from './ListaCartasComparar';


// MIN 12 DE VIDEO AÑADE ICON

class Sidebar extends React.Component {

  render() {

    return (

      <div className="sidebar_section">

        <Acordion title="FAVORITOS">
          <CartasSidebar />
        </Acordion>
        <Acordion title="COMPARACION">
          <ListaCartasComparar />
        </Acordion>
        <Acordion title="MAZOS">
          <MazosSidebar />
        </Acordion>

      </div>
    );
  }
}

export default Sidebar;