import React, { Component } from "react";
import "./OpcionMazosSidebar.css";
import { Link } from 'react-router-dom';
import { USER_KEY } from '../../conf/const';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'


class OpcionMazosSidebar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }

    }

    submit = (disabled) => {

        if (!disabled) {
            confirmAlert({
                message: `Estás seguro de que quieres borrar el mazo "${this.props.children.nombre}"? Recuerda que se borrarán todas las cartas en su interior.`,
                buttons: [
                    {
                        label: 'No'
                    },
                    {
                        label: 'Si',
                        onClick: () => {
                            this.props.remove(this.props.children.id)
                        }
                    }

                ]
            })
        }

    };


    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));
        // console.log(usuario.id);
        if (usuario !== null) return usuario.id;
        else return null;
    }




    // this.setState({ visible: false });

    render() {
        let ruta = window.location.pathname;

        return (
            <div className={this.props.mazo === "true" ? "opcion_mazo mazos" : "opcion_mazo"}>
                <div className="row m-0">
                    <div className="col-10">
                        <Link to={`/mazo/${this.props.children.id}`} className="w-100 boton-mazos">
                            <div className="mr-auto p-3">{this.props.children.nombre}</div>
                        </Link>
                    </div>
                    {ruta !== `/mazo/${this.props.children.id}`
                        ? (
                            <div className="col-2 d-flex justify-content-center align-items-center">
                                <span className="w-100 text-center" style={{ cursor: 'pointer' }} onClick={() => this.submit(false)}><i className="fa fa-times iconoTrash"></i></span>
                            </div>
                        )
                        : (
                            <div className="col-2 d-flex justify-content-center align-items-center">
                                <span className="w-100 text-center disabled" onClick={() => this.submit(true)}><i className="fa fa-times iconoTrash"></i></span>
                            </div>
                        )
                    }
                </div>
            </div>

        );
    }
}

OpcionMazosSidebar.defaultProps = {
    mazo: "true"
}

export default OpcionMazosSidebar;