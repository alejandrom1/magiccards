import React from 'react';
import OpcionCartasSidebar from './OpcionCartasSidebar';
import { FAV_KEY, USER_KEY } from '../../conf/const';
import { connect } from 'react-redux';

class CartasSidebar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cartas: [],
            recarga: false,
        }

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.recarga!==this.props.recarga){
            this._cargaComponente();
        }
      }

    componentDidMount(){
        this._cargaComponente();
    }

    _cargaComponente = () => {
        let idUsuario = this._getUsuarioId();
        this._getFav(idUsuario);
    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) return usuario.id;
        else return null;
    }
    
    _recargarPrincipal = () => {
        this.props.dispatch({
            type: 'RECARGA',
            item: !this.state.recarga,
        });
    }

    _getFav = (idUsuario) => {
        let favoritKey = JSON.parse(localStorage.getItem(FAV_KEY));
        

        if(favoritKey!==null){
            let cartas = favoritKey.filter(card => card.idUsuario===idUsuario).map(el=> el.carta);
            cartas.length>0 ? this.setState({cartas: cartas[0]}) : this.setState({cartas: cartas})
            
        }
    }

    _RemoveToFav = (card) => {
        let favoritos = [];
        favoritos = JSON.parse(localStorage.getItem(FAV_KEY));
        let userId = this._getUsuarioId();

        //Esto sería como decir que: "Si el FAV_Key ya ha sido creado entonces haz..." Así que en su interior
        //Tiene que haber lo de añadir al array de favoritos del usuario que coge previamente la carta. El objeto

        if (localStorage.getItem(FAV_KEY) !== null) {
            favoritos.forEach((el, index) => {
                if (userId === el.idUsuario) {
                    if (el.carta.filter(el => el.id === card.id).length > 0) {
                        el.carta.forEach((carta, indice) => {
                            if(carta.id === card.id) 
                            {
                                el.carta.splice(indice, 1);
                                localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                                this.setState({cartas: el.carta}, this._recargarPrincipal());
                            }
                            
                        })
                        
                    }
                }
            })   
        }
    }

    render() {
        if (this.state.cartas.length === 0) return <h6 className="text-center font-weight-bold">No hay cartas para mostrar</h6>;

        return (
            <>
                {
                    this.state.cartas.length > 0
                        ? this.state.cartas.map((cartas, index) => {
                            return (
                                <OpcionCartasSidebar remove={this._RemoveToFav} key={index}>
                                    {cartas}
                                </OpcionCartasSidebar>

                            );
                            
                        })
                        : null
                }
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        recarga: state.recarga
    }
  }


export default connect(mapStateToProps) (CartasSidebar);