import React, { Component } from "react";
import "./OpcionCartasSidebar.css";
import { USER_KEY } from '../../conf/const';


class OpcionCartasSidebar extends Component {

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) return usuario.id;
        else return null;
    }


    render() {
        return (
            <div className="opcion_card">
                <div className="row">
                    <div className="col-10">
                        {this.props.children.imageUrl !== undefined
                            ? <img alt="Imagen de la carta" className="img-fluid mr-2 img-card" src={this.props.children.imageUrl} />
                            : <img alt="Imagen por defecto" className="img-fluid mr-2 img-card" src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                        }
                        <span>{this.props.children.name}</span>
                    </div>
                    <div className="col-2">
                        <button onClick={() => this.props.remove(this.props.children)} className="btn" title="Borrar Carta"><i className="fa fa-times iconoTrash"></i></button>
                    </div>
                </div>
            </div>
        );
    }
}

export default OpcionCartasSidebar;