import React, { Component } from 'react';
import './Home.css';


export default class Textohome extends Component {
    render() {

        return (
            <div className="home">
                <div className="row justify-content-md-center">
                    <img alt="Logo_magic" className="img-fluid" style={{ height: '100%' }} src={process.env.PUBLIC_URL + '/img/logo-magic.png'} />
                </div>


                <div className="row justify-content-md-center">
                    <p>Proyecto realizado en un bootcamp de JS Full Stack Web Developer en: &#160;
                        <span>
                            <a className="aLink-home" target="_blank" rel="noopener noreferrer" href="https://fundacionesplai.org/">Fundación Esplai</a>
                        </span>
                    </p>
                </div>

                <div className="row justify-content-md-center">
                    <a target="_blank" rel="noopener noreferrer" href="https://fundacionesplai.org/"><img alt="logo fundacion esplai" className="image-home" src="https://i.ibb.co/y87tbNz/EJLMZhdg-400x400.png" /></a>
                </div>

                <br />




                <div className="row justify-content-md-center">
                    <div className="lista-linkedin">
                        <p>El proyecto ha sido realizado por:</p>

                        <ul>
                            <li>
                                <a href="https://www.linkedin.com/in/alexnietomendez/" target="_blank" rel="noopener noreferrer" className="boton-linkedin">Alex Nieto Méndez <i className="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/odasolalopez/" target="_blank" rel="noopener noreferrer" className="boton-linkedin">Oda Solà López <i className="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/alejandromg98/" target="_blank" rel="noopener noreferrer" className="boton-linkedin">Alejandro Moreno García <i className="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/InfoPMA/" target="_blank" rel="noopener noreferrer" className="boton-linkedin">Pablo Martínez Alcázar <i className="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <br />

                <div className="row justify-content-md-center">
                    <p>La principal herramienta de desarrollo ha sido ReactJS</p>
                </div>


                <div className="row justify-content-md-center">
                    <p><a className="aLink-home" rel="noopener noreferrer" href="https://docs.magicthegathering.io/" target="_blank">Enlace a la documentación de la API utilizada</a></p>
                </div>



            </div>

        )

    }


}

