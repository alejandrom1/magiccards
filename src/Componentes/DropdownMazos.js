import React, { Component } from 'react';
import { MAZO_KEY, USER_KEY } from '../conf/const';


class DropdownMazos extends Component {

    constructor(props) {
        super(props);

        this.state = {
            misMazos: [],
            cartaActual: this.props.cartaActual

        }


    }

    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));
        
        if (usuario !== null) return usuario.id;
        else return null;
    }


    _chargeMazos = () => {
        let idUser = this._getUsuarioId();

        const allListadoMazos = JSON.parse(localStorage.getItem(MAZO_KEY));

        if (allListadoMazos !== null) {
            let checkMazos = allListadoMazos.map(el => el.idUsuario);
            let checkMazos2 = checkMazos.includes(idUser);
            if (checkMazos2 !== false) {
                let userListado = allListadoMazos.filter(listado => listado.idUsuario === idUser);

                if (userListado !== undefined) {
                    if (idUser === userListado[0].idUsuario) {
                        let arrayMazos = userListado[0].mazos;
                        this.state.misMazos = arrayMazos; //Esto es ilegalisimo, pero así funciona, lo dejo así hasta que solucione el problema del bucle
                    }
                }
            }
        }
    }

    _addCartaMazo = (idMazo) => {
        let idUser = this._getUsuarioId();
        let allListadoMazos = JSON.parse(localStorage.getItem(MAZO_KEY));

        let userListado = allListadoMazos.filter(listado => listado.idUsuario === idUser);

        let mazoActual = userListado[0].mazos.filter(element => element.id === idMazo);

        let elMazo = mazoActual[0];
        let cartaActual = this.state.cartaActual;

        elMazo.cartas.push(cartaActual);
        localStorage.setItem(MAZO_KEY, JSON.stringify(allListadoMazos));

    }

    _mensajeNoHayMazos = () => {
        return(
            <div className="text-center font-weight-bold p-2">
                <p className="text-dark">No existen mazos</p>
            </div>
        );
    }


    render() {

        return (
            <div className="dropdown ml-1">
                <span onClick={this._chargeMazos()} className={`${this.props.styleClass}`}  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i className="fa fa-bars"></i>
                </span>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    {this.state.misMazos.length === 0
                        ? this._mensajeNoHayMazos()
                        : (this.state.misMazos.map((mazo) => {
                            return (<span className="dropdown-item" style={{cursor: 'pointer', fontSize: '15px'}} onClick={() => this._addCartaMazo(mazo.id)} key={mazo.id}>{mazo.nombre}</span>);
                        }))
                    }

                </div>
            </div>
        );
    }
}

export default DropdownMazos;