import React from 'react';
import { Switch, Route } from "react-router-dom";

import Login from "../Componentes/Login/Login";
import Sidebar from "./Sidebar/Sidebar";
import { USER_KEY } from '../conf/const';
import { login, home, sets, cards, cardById , comparar, mazoById, sidebar} from '../conf/routes';
import RequireAuth from './RequireAuth';
import MostrarSets from './MostrarSets';
import PrintarCards from './PrintarCards';
import DetalleCarta from './Detalle_carta';
import showCartasMazo from './showCartasMazo';
import Compara_Cartas from './Comparar_Cartas';


import Menu from './Menu';

//texto en home 
import Textohome from './Home';

class Main extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      logged: false
    }
  }

  _hasLogged = (log) => {
    this.setState({
      logged: log
    })

    if (!log) {
      localStorage.removeItem(USER_KEY);
    }

  }

  render() {

    const logged = localStorage.getItem(USER_KEY)!==null ? true : false;
    let datos = {
      ...this.props
    };

    return (
      <>
        <div>
          <Menu logged={this._hasLogged} />
          <div className="row mr-0">
            <div className={logged ? "col-8" : "col-12"}>
              <div className="m-auto pt-5" style={{ width: '90%' }}>
                <Switch>
                
                  <Route exact path={login()} >
                    <Login logged={this._hasLogged} />
                  </Route>
                  
                  <Route path={sets()} component={props => <RequireAuth {...props} Componente={MostrarSets} />} />
                  <Route path={cards()} component={props => <RequireAuth {...props} Componente={PrintarCards} />} />
                  <Route path={cardById()} component={props => <RequireAuth {...props} Componente={DetalleCarta} />} />
                  <Route path={comparar()} component={props => <RequireAuth {...props} Componente={Compara_Cartas} />} />
                  <Route path={mazoById()} component={props => <RequireAuth {...props} Componente={showCartasMazo} />} />
                  <Route path={home()} component={props => <RequireAuth {...props} Componente={Textohome} />} />

                </Switch>
              </div>
            </div>

            <div className={logged ? "col-4 pt-5" : "col-4 pt-5"}>
              <Route path={sidebar()} component={() => <RequireAuth {...datos} Componente={Sidebar} />} />
            </div>
          </div>
        </div>


      </>
    );
  }
}

export default Main;