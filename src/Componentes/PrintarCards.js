import React, { Component } from 'react'
import AccesoApi from '../Modelo/AccesoApi';
import "./PrintarCards.css";
import { FAV_KEY, USER_KEY, Compara_KEY } from "../conf/const";
import { Link } from 'react-router-dom';
import Cargando from '../Componentes/Globales/Cargando';
import DropdownMazos from './DropdownMazos';
import { sets } from '../conf/routes';
import { connect } from 'react-redux';

class PrintarCards extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cards: [],
            idsCartasEnComparar: [],
            idsCartasEnFavoritos: [],
            reciboCartas: false
        }

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.recarga!==this.props.recarga){
            this._cargarListaCartasEnComparar();
            this._cargarListaCartasEnFavoritos();
        }
      }

    componentDidMount() {
        const params = this.props.match.params;
        this._getCardsByCodeSet(params.code);
        this._cargarListaCartasEnComparar();
        this._cargarListaCartasEnFavoritos();
    }

    _getCardsByCodeSet = (code) => {

        AccesoApi.leerApi(`cards?set=${code}`)
            .then(response => {
                this.setState({ cards: response.cards, reciboCartas: true });
            })
            .catch(error => console.log(error));

    }
    _getUsuarioId = () => {
        let usuario = JSON.parse(localStorage.getItem(USER_KEY));

        if (usuario !== null) {
            return usuario.id
        }

        return null;
    }

    _cargarListaCartasEnComparar = () => {
        let allCartasEnComparar = JSON.parse(localStorage.getItem(Compara_KEY));
        let usuarioId = this._getUsuarioId();

        if (allCartasEnComparar !== null) {
            let cartasEnComparar = allCartasEnComparar.filter(el => el.idUsuario === usuarioId).map(el => el.carta);
            let idCartasEnComparar = []

            if (cartasEnComparar[0] !== undefined) {
                idCartasEnComparar = cartasEnComparar[0].map(el => el.id);
            }
            
            this.setState({idsCartasEnComparar: idCartasEnComparar});
        }
    }

    _cargarListaCartasEnFavoritos = () => {
        let allCartasEnFavoritos = JSON.parse(localStorage.getItem(FAV_KEY));
        let usuarioId = this._getUsuarioId();

        if (allCartasEnFavoritos !== null) {
            let cartasEnFavoritos = allCartasEnFavoritos.filter(el => el.idUsuario === usuarioId).map(el => el.carta);
            let idCartasEnFavoritos = [];

            if (cartasEnFavoritos[0] !== undefined) {
                idCartasEnFavoritos = cartasEnFavoritos[0].map(el => el.id);
            }

            this.setState({idsCartasEnFavoritos: idCartasEnFavoritos});
        }
    }

    _recargarSidebar = () => {
        this.props.dispatch({
            type: 'RECARGA',
            item: !this.state.recarga,
        });
    }

    Add_comparar = (carta) => {
        let cartas_omparar = [];
        cartas_omparar = JSON.parse(localStorage.getItem(Compara_KEY));
        let userId = this._getUsuarioId();
        let usuarioNoExiste = true;

        if (localStorage.getItem(Compara_KEY) !== null) {
            cartas_omparar.forEach(el => {
                if (userId === el.idUsuario) {
                    usuarioNoExiste = false;
                    if (el.carta.filter(el => el.id === carta.id).length > 0) {


                        el.carta.forEach((card, indice) => {
                            if (card.id === carta.id) {
                                el.carta.splice(indice, 1);
                                this._recargarSidebar();
                                localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                                this._cargarListaCartasEnComparar();
                            }
                        })


                    }
                    else {
                        el.carta.push(carta);
                        this._recargarSidebar();
                        localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                        this._cargarListaCartasEnComparar();
                    }
                }
            })
            if (usuarioNoExiste) {
                let nuevaCard = { idUsuario: userId, carta: [carta] };
                cartas_omparar.push(nuevaCard);
                this._recargarSidebar();
                localStorage.setItem(Compara_KEY, JSON.stringify(cartas_omparar));
                this._cargarListaCartasEnComparar();
            }
        } else {
            let nuevaCarta_Comp = [{ idUsuario: userId, carta: [carta] }];
            this._recargarSidebar();
            localStorage.setItem(Compara_KEY, JSON.stringify(nuevaCarta_Comp));
            this._cargarListaCartasEnComparar();
        }
    }
    _addToFav = (card) => {
        let favoritos = [];
        favoritos = JSON.parse(localStorage.getItem(FAV_KEY));
        let userId = this._getUsuarioId();
        let usuarioNoExiste = true;

        //Esto sería como decir que: "Si el FAV_Key ya ha sido creado entonces haz..." Así que en su interior
        //Tiene que haber lo de añadir al array de favoritos del usuario que coge previamente la carta. El objeto

        if (localStorage.getItem(FAV_KEY) !== null) {
            favoritos.forEach((el, index) => {
                if (userId === el.idUsuario) {
                    usuarioNoExiste = false;
                    if (el.carta.filter(el => el.id === card.id).length > 0) {


                        el.carta.forEach((carta, indice) => {
                            if (carta.id === card.id) {
                                el.carta.splice(indice, 1);
                                localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                                this._recargarSidebar();
                                this._cargarListaCartasEnFavoritos();
                            }
                        })


                    }
                    else {
                        el.carta.push(card); //Dentro del FKey que ya existe, dentro de "Carta", le introduzco la siguiente "card"
                        localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                        this._recargarSidebar();
                        this._cargarListaCartasEnFavoritos();
                    }
                }
            })
            if (usuarioNoExiste) {
                let nuevaCard = { idUsuario: userId, carta: [card] };
                favoritos.push(nuevaCard);
                localStorage.setItem(FAV_KEY, JSON.stringify(favoritos));
                this._recargarSidebar();
                this._cargarListaCartasEnFavoritos();
            }
        }
        else {
            let nuevaCarta_Fav = [{ idUsuario: userId, carta: [card] }];
            localStorage.setItem(FAV_KEY, JSON.stringify(nuevaCarta_Fav));
            this._recargarSidebar();
            this._cargarListaCartasEnFavoritos();
        }
    }

    _montarCard = (carta) => {

        let cartaEnComparar = this.state.idsCartasEnComparar.includes(carta.id);
        let cartaEnFavoritos = this.state.idsCartasEnFavoritos.includes(carta.id);
        
        if (carta.id !== undefined) {
            return (

                <div className="card" key={carta.id}>
                    <Link to={`/card/${carta.id}`} style={{ textDecoration: 'none', margin: '.5rem' }}>
                        <p>{carta.name}</p>
                        {carta.imageUrl === undefined
                            ? <img className="img-fluid" alt={carta.name} src="https://i.ibb.co/cNjQCSV/carta-Por-Defecto3.png" />
                            : <img className="img-fluid" alt={carta.name} src={carta.imageUrl} />
                        }
                    </Link>
                    <div className="d-flex justify-content-around p-2 menu-icons">
                        <span onClick={() => this._addToFav(carta)}><i className={cartaEnFavoritos ? "fa fa-heart color-icons-selected" : "fa fa-heart-o"}></i></span>
                        <span onClick={() => this.Add_comparar(carta)}><i className={cartaEnComparar ? "fa fa-balance-scale color-icons-selected" : "fa fa-balance-scale"}></i></span>
                        <DropdownMazos styleClass="" cartaActual={carta} />
                    </div>
                </div>

            );
        }
    }

    render() {

        if (!this.state.reciboCartas) return <Cargando />;

        const nameSet = this.state.cards.length !== 0 ? this.state.cards[0].setName : "Lo sentimos...";
        return (
            <>
                <div className="name-set">
                    <Link to={sets()} className="btn btn-danger position-absolute bottom ml-4">Volver</Link>
                    <h2 className="text-center text-white">{nameSet}</h2>
                </div>

                <div className="cards">
                    {this.state.cards.length === 0
                        ? <h3>Faltand datos sobre este set de cartas...</h3>
                        : this.state.cards.map(carta => this._montarCard(carta))
                    }

                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        recarga: state.recarga
    }
  }


export default connect(mapStateToProps)(PrintarCards);