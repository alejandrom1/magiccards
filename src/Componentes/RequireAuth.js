import React from 'react';
import {Redirect} from 'react-router-dom';
import { USER_KEY } from '../conf/const';
import { login } from '../conf/routes';

export default function RequireAuth ({Componente, ...props}){
    // console.log(localStorage.getItem(USER_KEY));
        if(localStorage.getItem(USER_KEY)===null){
            return <Redirect to={login()}/>
        }
        return <Componente {...props}/>;
        
    
}