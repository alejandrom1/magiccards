import React from "react";

import { Redirect } from "react-router-dom";
import { USER_KEY } from "../conf/const";
import { login } from "../conf/routes";

export default function Logout(props) {

        localStorage.removeItem(USER_KEY)
        props.logged(false);
        return <Redirect to={login()} />

}