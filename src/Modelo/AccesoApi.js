let API_MAGIC = `https://api.magicthegathering.io/v1/`;

class AccesoApi {

    static async leerApi(param) {
        let ruta = API_MAGIC + param;
        return this.accederApi(ruta);
    }

    static async accederApi(ruta){
        return await fetch(ruta)
        .then(res => res.json())
        .then(
            results=> {
                return results;
            }
        )
    }

}

export default AccesoApi;
