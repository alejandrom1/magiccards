
const appReducer = (state = {
    recarga: false //Boleano que se encargará de renderizar el sidebar
}, action) => {

    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {

        case 'RECARGA':
            newState.recarga = !newState.recarga;
            return newState;

        default:
            return state;
    }

}
export default appReducer;